require('babel-register');
require('babel-polyfill');
require('dotenv').config();
const HDWalletProvider = require("@truffle/hdwallet-provider");

const infuraUrl = (network, projectID) => `https://${network}.infura.io/v3/${projectID}`;

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    // testnets
    // properties
    // network_id: identifier for network based on ethereum blockchain. Find out more at https://github.com/ethereumbook/ethereumbook/issues/110
    // gas: gas limit
    // gasPrice: gas price in gwei
    ropsten: {
      provider: () => new HDWalletProvider(process.env.MNEMONIC, infuraUrl('raposten', process.env.INFURA_PROJECT_ID)),
      network_id: 3,
      gas: 3000000,
      gasPrice: 10000000000
    },
    kovan: {
      provider: () => new HDWalletProvider(process.env.DEMO_ACCOUNT, infuraUrl('kovan', process.env.INFURA_PROJECT_ID)),
      network_id: 42,
      gas: 3000000,
      gasPrice: 10000000000
    },
    rinkeby: {
      provider: () => new HDWalletProvider(process.env.MNEMONIC, infuraUrl('rinkeby', process.env.INFURA_PROJECT_ID)),
      network_id: 4,
      gas: 3000000,
      gasPrice: 10000000000
    },
    // main ethereum network(mainnet)
    // main: {
    //   provider: () => new HDWalletProvider(process.env.MNEMONIC, infuraUrl('mainnet', process.env.INFURA_PROJECT_ID)),
    //   network_id: 1,
    //   gas: 3000000,
    //   gasPrice: 10000000000
    // }
  },
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/abis/',
  compilers: {
    solc: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  }
}
