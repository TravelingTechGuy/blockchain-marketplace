# My first Blockchain DApp

This repo follows a tutorial at [DApp University](https://www.dappuniversity.com/articles/how-to-build-a-blockchain-app) and the [video of the course](https://www.youtube.com/watch?v=VH9Q2lf2mNo).

The purpose: build a market place runnning on the Ethereum blockchain.  
The backend is a Solidity contract and the frontend is React.
