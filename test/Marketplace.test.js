require('chai').use(require('chai-as-promised')).should();
const Marketplace = artifacts.require("./Marketplace.sol");

const marketPlaceName = 'My Marketplace',
  productName = 'iPhone',
  productPrice = web3.utils.toWei('1', 'ether');

contract('Marketplace', ([deployer, seller, buyer]) => {
  let marketplace;
  before(async () => {
    marketplace = await Marketplace.deployed();
  })
  
  describe('deployment', async () => {
    it('deploys successfuly', async () => {
      const address = await marketplace.address;
      assert.exists(address);
      assert.notEqual(address, 0x0);
    });

    it('has a name', async () => {
      const name = await marketplace.name();
      assert.equal(name, marketPlaceName);
    });
  });

  describe('products', async () => {
    let result, productCount;
    
    before(async () => {
      result = await marketplace.createProduct(productName, productPrice, {from: seller});
      productCount = await marketplace.productCount();
    })

    it('creates a product', async () => {
      //Success
      assert.equal(productCount, 1);
      // console.log(result.logs);
      const event = result.logs[0].args;
      assert.equal(event.id.toNumber(), productCount, 'id is correct');
      assert.equal(event.name, productName, 'name is correct');
      assert.equal(event.price, productPrice, 'price is correct');
      assert.equal(event.owner, seller, 'owner is correct');
      assert.isFalse(event.purchased, 'purchased is correct');

      //Failure
      await marketplace.createProduct('', productPrice, {from: seller}).should.be.rejected;
      await marketplace.createProduct(productName, 0, {from: seller}).should.be.rejected;
    });

    it('lists products', async () => {
      const product = await marketplace.products(productCount);
      assert.equal(product.id.toNumber(), productCount, 'id is correct');
      assert.equal(product.name, productName, 'name is correct');
      assert.equal(product.price, productPrice, 'price is correct');
      assert.equal(product.owner, seller, 'owner is correct');
      assert.isFalse(product.purchased, 'purchased is correct');
    });

    it('sells products', async () => {
      let oldSellerBalance = await web3.eth.getBalance(seller);
      oldSellerBalance = new web3.utils.BN(oldSellerBalance);
      //Success
      result = await marketplace.purchaseProduct(productCount, {from: buyer, value: productPrice});
      const event = result.logs[0].args;
      // console.log(event)
      assert.equal(event.id.toNumber(), productCount, 'id is correct');
      assert.equal(event.name, productName, 'name is correct');
      assert.equal(event.price, productPrice, 'price is correct');
      assert.equal(event.owner, buyer, 'owner is correct');
      assert.isTrue(event.purchased, 'purchased is correct');

      //Check that the seller received the funds
      let newSellerBalance = await web3.eth.getBalance(seller);
      newSellerBalance = new web3.utils.BN(newSellerBalance);
      let price = new web3.utils.BN(productPrice);
      let expectedBallance = oldSellerBalance.add(price);
      // console.log(web3.utils.fromWei(oldSellerBalance, 'ether'), web3.utils.fromWei(newSellerBalance, 'ether'), web3.utils.fromWei(price, 'ether'));
      assert.equal(newSellerBalance.toString(), expectedBallance.toString(), 'seller balance shows price');
      
      //Failure
      //buy product that doesn't exist
      await marketplace.purchaseProduct(99, {from: buyer, value: productPrice}).should.be.rejected;
      //pass less ether than the price
      await marketplace.purchaseProduct(productCount, {from: buyer, value: web3.utils.toWei('1.9', 'ether')}).should.be.rejected;
      //Deployer tries to buy the product, i.e., product can't be purchased twice
      await marketplace.purchaseProduct(productCount, { from: deployer, value: productPrice}).should.be.rejected;
      //Buyer tries to buy again, i.e., buyer can't be the seller
      await marketplace.purchaseProduct(productCount, { from: buyer, value: productPrice}).should.be.rejected;
    });
  });
});
