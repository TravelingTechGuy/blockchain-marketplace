import React, {useState} from 'react';

export default ({createProduct}) => {
  const [product, setProduct] = useState(null);

  return (
    <>
      <p/>
      <form onSubmit={(event) => {
        event.preventDefault();
        const price = window.web3.utils.toWei(product.price.toString(), 'Ether');
        createProduct(product.name, price);
      }}>
        <div className="form-group mr-sm-2">
          <input
            id="productName"
            type="text"
            onChange={e => setProduct({...product, name: e.target.value })}
            className="form-control"
            placeholder="Product Name"
            required />
        </div>
        <div className="form-group mr-sm-2">
          <input
            id="productPrice"
            type="text"
            onChange={e => setProduct({...product, price: e.target.value })}
            className="form-control"
            placeholder="Product Price"
            required />
        </div>
        <button type="submit" className="btn btn-primary">Add Product</button>
      </form>
    </>
  );
};
