import React from 'react';
import {Alert} from 'react-bootstrap';

export default ({error}) => {
  const onClose = () => window.location.reload();

  return (
    <div className="container-fluid mt-5">
      <div className="row">
        <main role="main" className="col-lg-12 d-flex text-center">
          <Alert variant="danger" onClose={onClose} dismissible>
            <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
            <p>{error}</p>
          </Alert>
        </main>
      </div>
    </div>
  );
}
