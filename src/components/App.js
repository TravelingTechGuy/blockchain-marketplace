import React, { useState, useEffect } from 'react';
import Web3 from 'web3';
import Marketplace from '../abis/Marketplace';
import Navbar from './Navbar';
import Main from './Main';
import Error from './Error';
import './App.css';

export default () => {
  const [network, setNetwork] = useState('');
  const [account, setAccount] = useState('');
  const [marketplace, setMarketplace] = useState(null);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  
  /**
   * On mount, connect to web3, get account, and blockchain data
   */
  useEffect(() => {
    const init = async () => {
      setLoading(true);
      await loadWeb3();
      await loadAccount();
      await loadBlockchainData();
      setLoading(false);
    };
    
    init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   * On marketplace change, load products
   */
  useEffect(() => {
    marketplace && getProducts();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [marketplace]);

  /**
   * Load web3 and hook up events
   */
  const loadWeb3 = async () => {
    setError(null);
    if(window.ethereum) {
      window.ethereum
        .on('accountsChanged', accounts => setAccount(accounts[0]))
        .on('networkChanged', networkId => loadBlockchainData(networkId))  
        .autoRefreshOnNetworkChange = false;
      window.web3 = new Web3(window.ethereum);
      await window.ethereum.enable();
    }
    else if(window.web3) {
      window.web3 = new Web3(window.web3.currentProvider);
    }
    else {
      setError('Non-Ethereum browser detected. You should consider trying MetaMask!');
    }
  };

  /**
   * Get account address from web3 provider
   */
  const loadAccount = async () => {
    const web3 = window.web3;
    const accounts = await web3.eth.getAccounts();
    setAccount(accounts[0]);
  }

  /**
   * Get network data and contract
   */
  const loadBlockchainData = async (networkId = null) => {
    const web3 = window.web3;
    networkId = networkId || await web3.eth.net.getId();
    const networkType = await web3.eth.net.getNetworkType();
    setNetwork(networkType);
    const networkData = Marketplace.networks[networkId];
    if(networkData) {
      const abi = Marketplace.abi;
      const contractAddress = networkData.address;
      const marketplace = new web3.eth.Contract(abi, contractAddress);
      setMarketplace(marketplace);
      setError(null);
    }
    else {
      setError(`Marketplace contract not deployed to detected network ${networkType}`);
    }
  };

  /**
   * Get list of products from contract
   */
  const getProducts = async () => {
    const productCount = await marketplace.methods.productCount().call();
    let promises = [];
    for(var i = 1; i <= productCount; i++) {
      promises.push(marketplace.methods.products(i).call());
    }
    Promise.all(promises)
      .then(products => setProducts(products))
      .catch(setError);
  };

  /**
   * Create new product in the marketplace
   * @param {string} name product name
   * @param {string} price product price in ETH
   */
  const createProduct = (name, price) => {
    setLoading(true);
    marketplace.methods.createProduct(name, price)
      .send({ from: account })
      .once('receipt', receipt => {
        console.log(receipt);
      })
      .on('error', setError)
      .then(async () => {
        await getProducts();
        setError(null);
        setLoading(false);
      });
  };

  /**
   * Purchase product
   * @param {string} id product id
   * @param {string} price product price in ETH
   */
  const purchaseProduct = (id, price) => {
    setLoading(true);
    marketplace.methods.purchaseProduct(id)
      .send({ from: account, value: price })
      .once('receipt', receipt => {
        console.log(receipt);
      })
      .on('error', setError)
      .then(async () => {
        await getProducts();
        setError(null);
        setLoading(false);
      });
  };
  
  return (
    <div>
      <Navbar account={account} network={network} />
      {
        error ? <Error error={error} /> : <Main props={{loading, products, createProduct, purchaseProduct}} />
      }
    </div>
  );
}
