import React from 'react';
import {Spinner, Tab, Tabs} from 'react-bootstrap';
import CreateProduct from './CreateProduct';
import PurchaseProduct from './PurchaseProduct';

export default ({props}) => {
  const {loading, products, createProduct, purchaseProduct} = props;
  return (
    <div className="container-fluid mt-5">
      <div className="row">
        <main role="main" className="col-lg-12 d-flex text-center">
          <div id="content">
            {loading 
              ? 
              <Spinner animation="border" variant="primary" /> 
              : 
              <Tabs defaultActiveKey="buy">
                <Tab eventKey="buy" title="Buy Product">
                  <PurchaseProduct products={products} purchaseProduct={purchaseProduct} />
                </Tab>
                <Tab eventKey="add" title="Add Product">
                  <CreateProduct createProduct={createProduct} />
                </Tab> 
              </Tabs>
            }
          </div>
        </main>
      </div>
    </div>
  );
};
