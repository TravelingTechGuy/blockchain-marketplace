import React from 'react';
import {Navbar} from 'react-bootstrap';
import logo from '../logo.png';
import './Navbar.css';

export default ({account, network}) => {
  return (
    <Navbar bg="dark" variant="dark" sticky="top">
      <Navbar.Brand
        as="a"
        href="http://www.dappuniversity.com/bootcamp"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img
          alt="logo"
          src={logo}
          width="25"
          height="25"
          className="d-inline-block align-top"
        />
        {' Blockchain Marketplace'}
      </Navbar.Brand>
      <Navbar.Collapse className="justify-content-end">
        <Navbar.Text>
          <span id="account">{account ? account : 'loading'}</span>&nbsp;
          <span id="network">{network ? ` (${network})` : ''}</span>
        </Navbar.Text>
      </Navbar.Collapse>
    </Navbar>
  );
}
