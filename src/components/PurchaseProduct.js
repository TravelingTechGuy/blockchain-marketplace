import React from 'react';
import {Badge, Button} from 'react-bootstrap';

export default ({products, purchaseProduct}) => {
  return (
    <>
      {products.length ? 
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Owner</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody id="productList">
          {
            products.map(product => 
              <tr key={product.id.toString()}>
                <th scope="row">{product.id}</th>
                <td>{product.name}</td>
                <td>{window.web3.utils.fromWei(product.price.toString(), 'Ether')} Ether</td>
                <td>{product.owner}</td>
                <td>
                  { !product.purchased
                    ? <Button
                        variant="success"
                        size="sm"
                        onClick={() => purchaseProduct(product.id, product.price)}
                      >
                        Buy
                      </Button>
                    : <Badge variant="danger" size="sm">Purchased!</Badge>
                  }
                </td>
              </tr>  
            )
          }
          </tbody>
        </table>
        :
        <h3>No products - please add one</h3>
      }
    </>
  );
};
