pragma solidity ^0.5.0;

contract Marketplace {
  string public name;
  uint public productCount = 0;
  mapping(uint => Product) public products;

  struct Product {
    uint id;
    string name;
    uint price;
    address payable owner;
    bool purchased;
  }

  event ProductCreated(
    uint id,
    string name,
    uint price,
    address payable owner,
    bool purchased
  );

  event ProductPurchased(
    uint id,
    string name,
    uint price,
    address payable owner,
    bool purchased
  );

  constructor() public {
    name = "My Marketplace";
  }

  function createProduct(string memory _name, uint _price) public {
    //check parameters correct
    require(bytes(_name).length > 0, "name cannot be empty");
    require(_price > 0, "price has to be greater than 0");
    //increment product count
    productCount++;
    //create the new product
    products[productCount] = Product(productCount, _name, _price, msg.sender, false);
    //trigger a bc event
    emit ProductCreated(productCount, _name, _price, msg.sender, false);
  }

  function purchaseProduct(uint _id) public payable {
    //make sure product is valid
    require(_id > 0 && _id <= productCount, "product id must be valid");
    //fetch product
    Product memory _product = products[_id];
    //make sure there's enoygh ether in the transaction
    require(msg.value >= _product.price, "value lower than price");
    //make sure product hasn't been purchased yet
    require(!_product.purchased, "cannot buy a purchased product");

    //fetch owner
    address payable _seller = _product.owner;
    //make sure the buyer is not the seller
    require(_seller != msg.sender, "buyer cannot be the seller");

    //transfer ownership to buyer
    _product.owner = msg.sender;
    //purchase it
    _product.purchased = true;
    products[_id] = _product;
    //pay the seller by passing Ether
    address(_seller).transfer(msg.value);
    //trigger event
    emit ProductPurchased(_product.id, _product.name, _product.price, _product.owner, _product.purchased);
  }
}
